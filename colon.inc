%define end 0

%macro colon 2
%%next:
dq end
%ifstr %1
    db %1, 0
    %else
		%fatal "ERROR :{-{ "
	%endif
%ifid %2
    %2:
    %define end %%next
    %else
	    %fatal "ERROR }-{: "
%endmacro

