ASM = nasm
FLAGS = -f elf64 -o

LD = ld -o

.PHONY: clean

ne-hochu-na-dopsu: main.o lib.o dict.o
	$(LD) $@ $^
	
main.o: main.asm words.inc colon.inc lib.inc
	$(ASM) $(FLAGS) $@ main.asm
	
lib.o: lib.asm
	$(ASM) $(FLAGS) $@ $^

dict.o: dict.asm
	$(ASM) $(FLAGS) $@ $^
	
clean:
	rm -rf main *.o

	

